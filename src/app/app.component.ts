import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'authenticationRouteProtection';
  constructor() { }
  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDnzZ6V3ABLa6nNuC5s8Sj5S_wCbtBEyCk',
      authDomain: 'authentication-f8711.firebaseapp.com'
    });
  }
}

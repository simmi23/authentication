import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.css']
})
export class AfterLoginComponent implements OnInit {
   appData: any;
   auth ;
  constructor(public authservice: AuthService, private dataservice: DataService, private router: Router) { }

  ngOnInit() {
   // this.auth = this.authservice.isAuthenticated();
  }
  showDataFromFirebase() {
      this.appData = this.dataservice.getServersDirectlyAddedtoFirebase();
  }
  logOut() {
    this.authservice.logOut();
    this.router.navigate(['/signin']);
  }
}

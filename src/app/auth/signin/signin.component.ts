import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authservice: AuthService, private router: Router) { }

  ngOnInit() {
  }
  onSubmitsignIn(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    // console.log(email + password);
    this.authservice.signInDetails(email, password);
    this.router.navigate(['/data']);
  }

}

import * as firebase from 'firebase';


import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
export class AuthService {
  token: string;
  // firebase accepts the password atleast of six characters
  signUpDetails(email: string, password: string) {
     firebase.auth().createUserWithEmailAndPassword(email, password)
       .catch(
         error => console.log(error)
       );
  }

  signInDetails(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        response => {
          firebase.auth().currentUser.getIdToken().then(
            (token: string) => this.token = token
          );
        }
      )
      .catch(
        error => console.log(error)
      );
 }
  getToken() {
    // getIdToken is an asynchronous action because firebase does not retrieve the token from local storage
    // which is asynchrous and check token valid . if it expires it generate new token.
    firebase.auth().currentUser.getIdToken().then(
      (token: string) => this.token = token
    );
    console.log(this.token);
    return this.token;
  }

  isAuthenticated() {
    // console.log('auth', this.token !== null)
    return this.token !== null;

  }

  logOut() {
    firebase.auth().signOut();
    this.token = null;
  }
}

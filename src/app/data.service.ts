import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth/auth.service';

@Injectable()
export class DataService {
  constructor(private http: Http, private authservice: AuthService) { }
  getServersDirectlyAddedtoFirebase() {
    const token = this.authservice.getToken();
    return this.http.get('https://authentication-f8711.firebaseio.com/test.json?auth=' + token).map(
      (response: Response) => {
        return response.json();
      }
    );
  }

}
